import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';



const styles = {
  root: {
    flexGrow: 1,
    
  },
  grow: {
    flexGrow: 1,
    fontWeight:550,
    fontSize:20,
    letterSpacing:'0.03em',
    marginLeft:20,
    fontFamily: "Roboto,Avenir,Helvetica,Arial,sans-serif"
  },
  grow2: {
    flexGrow: 1,
    fontWeight:450,
    fontSize:17,
    letterSpacing:'0.03em',
    marginLeft:20,
    fontFamily: "Roboto,Avenir,Helvetica,Arial,sans-serif"
  },
  Main:{
    backgroundColor:"#2c4a70",
  },
  logo:{
      width:32.99,
      height:50,
      marginLeft:28.5,
      marginBottom:7
  }
};

function Navbar(props) {
  const { classes } = props;
  const [open, setOpen] = React.useState(false);
  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }
  
  return (
    <div className={classes.root}>
      <AppBar className={classes.Main} position="static">
        <Toolbar className={classes.ToolbarContent}>
          <img className={classes.logo} src={process.env.PUBLIC_URL + '/logo.png'} alt="logo"/>
          <Typography variant="h6" color="inherit" className={classes.grow}>
            IntuTrack
          </Typography>
          <Button variant="h6" onClick={handleClickOpen} color="inherit" className={classes.grow2}>
            How It Works
          </Button>
          <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Instructions"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <List>
              <ListItem>1. Fill In the Vehicle Category Form. You can add Multiple Categories. Submit It.</ListItem>
              <ListItem>2. Check The Details Entered.You can Refill the Form. If you are sure, Click on Upload</ListItem>
              <ListItem>3. Choose the file, you want to Upload. Click on Confirm</ListItem>
              <ListItem>4. Once you click on Confirm, The process starts. It takes a couple of minutes</ListItem>
              <ListItem>5. When process is completed, the Map will be rendered</ListItem>
              <ListItem>6. You can select Vehicles, See Unassigned Vehicles and assign Them.</ListItem>
            </List>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            OK
          </Button>
        </DialogActions>
      </Dialog>
        </Toolbar>
      </AppBar>
    </div>
  );
}

Navbar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Navbar);