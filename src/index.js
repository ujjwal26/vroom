import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Popupexp from './Popup'
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { rawReducer } from './store/reducer';

// const initialState = {
//         count: 0
//       };

// function reducer(state = initialState, action) {
//         console.log('reducer', state, action);
//         return state;
//       }
// const store = createStore(reducer);
// store.dispatch({ type: "INCREMENT" });
// store.dispatch({ type: "INCREMENT" });
// store.dispatch({ type: "DECREMENT" });
// store.dispatch({ type: "RESET" });
const store = createStore(
        rawReducer,
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
      );
console.log(store.getState())


const routing=(
        <Provider store={store}>
        <Router >
        <Route path="/" component={Popupexp} />
        {/* <Route path="/spreadsheet" component={Spreadsheet} /> */}
        {/* <Route path="/jsonexcel" component={JsonExcel} />
        <Route path="/map" component={Test} />
        <Route path="/modify" component={AssignVehicles} />
        <Route path="/socket" component={SocketJS} /> */}
        </Router>
        </Provider>
)

ReactDOM.render(routing, document.getElementById('root'));
serviceWorker.unregister();
