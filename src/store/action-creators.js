import {SEND_RAW_TO_STORE} from './action-types'

export const sendRawToStore=(rawdata)=>({
    type:SEND_RAW_TO_STORE,
    rawdata
})