import React, { Component } from 'react';
import XLSX from 'xlsx';
import { make_cols } from './MakeColumns';
import {OutTable, ExcelRenderer} from 'react-excel-renderer';
import './excel-2007.css';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import axios from 'axios';
import openSocket from 'socket.io-client'
 import { connect } from 'react-redux';
 import {sendRawToStore} from './store/action-creators';
 


class Convert extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: {},
      data: [],
      cols: [],
      rows:[],
      rowsRender:[],
      fileCheck:false,
      dataTobeModified:null,
      response: null,
      endpoint: "http://192.168.0.154:8888/api"
     
    }
    this.handleFile = this.handleFile.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.clearQueue=this.clearQueue.bind(this);
    this.sendAxiosExcel=this.sendAxiosExcel.bind(this);
  }
  clearQueue(){
    window.location.reload();
  }
  sendAxiosExcel(){
   console.log(this.props.addedInputs)
  
  
   var b=this.props.addedInputs.map((element)=>{var x=element.startTime.split(":"); var y=element.endTime.split(":");
  element.time= [(x[0]*60*60+x[1]*60),(y[0]*60*60+y[1]*60)] ;
  element.capacity=parseInt(element.capacity)
  element.numberOfVehicles=parseInt(element.numberOfVehicles)
  console.log(element)
  })
  axios.post("http://192.168.0.154:8888/api", {vehicles:b,data: this.state.data})
     .then((res)=>{console.log(res);
    if(res.status==200){
      const socket = openSocket('http://192.168.0.154:8888/')

      console.log('inside 200')
      socket.on("fromAPI", data => {console.log(data);
      this.props.onSelectRawdata(data);
      })

    
    }
    else{
      console.log("error");
    }
  })
  console.log(this.props.count)
     
   }

  handleChange(e) {
    const files = e.target.files;
    if (files && files[0]) this.setState({ file: files[0] });
    let fileObj = e.target.files[0];

    ExcelRenderer(fileObj, (err, resp) => {
        if(err){
          console.log(err);            
        }
        else{
          this.setState({
           fileCheck:true,
            rows: resp.rows
          });
          this.handleFile();  
        }
      });  
                 
  };
 
  handleFile() {
    /* Boilerplate to set up FileReader */
    const reader = new FileReader();
    const rABS = !!reader.readAsBinaryString;
    let objectData={};
    reader.onload = (e) => {
      /* Parse data */
      const bstr = e.target.result;
      const wb = XLSX.read(bstr, { type: rABS ? 'binary' : 'array', bookVBA : true });
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_json(ws);
      /* Update state */
      var mainData=null;

      /*Global Var*/
      

  
      this.setState({ data: data,rowsRender:this.state.rows, cols: make_cols(ws['!ref']) },
       () => {
          mainData=JSON.stringify(this.state.data, null, 2)
        // console.log(JSON.stringify(this.state.data, null, 2));
      });
      var count=0;
      // var excelArray=[];
     this.state.data.forEach(element=>{
        element.activityId=element.__rowNum__-1;
        // element.service=0;
        // element.arrival=0;
        // element.vehicle="Unassigned";
         objectData[element.activityId] = element;
        //  excelArray.push(element)
       })
     
       
       console.log(this.state.data)
      this.setState({
        dataTobeModified:objectData,
        // excelArray:excelArray
      })
      // console.log(this.state.excelArray);
      console.log(this.props.addedInputs)
      this.props.sendRawToStore(this.state); 
    };

    
    if (rABS) {
      reader.readAsBinaryString(this.state.file);
    } else {
      reader.readAsArrayBuffer(this.state.file);
      };
      
  }
 
  render() {
    if(this.state.response==null)
    {const buttonStyle = {
      backgroundColor:"#2c4a70",
      color:"white"
    };
    
    console.log(this.state.data)
    return (
      <div>
        <label htmlFor="file">Upload an excel to Process Triggers</label>
        <br />
        <input type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" className="form-control" id="file" onChange={this.handleChange} />
        <br />

        {(this.state.fileCheck)?
       <div>
        <div className="preview-div scroll">
        <OutTable data={this.state.rowsRender} columns={this.state.cols} tableClassName="ExcelTable2007" tableHeaderRowClass="heading" />
        </div>
        <Grid container spacing={16}>
          <Grid item xs={3}></Grid>
          <Grid item xs={3}></Grid>
        <Grid item xs={3}>
          <Button variant="contained" color="primary" style={buttonStyle} onClick={this.sendAxiosExcel} >Confirm</Button>
        </Grid>
        <Grid item xs={3}>
          <Button variant="contained" style={buttonStyle} className="btn upload-btn" onClick={this.clearQueue} >Clear</Button>
        </Grid>
        <div style={{height:'0px'}}>
          {/* <Clustering dataToBeModified={this.state.dataTobeModified}/> */}
          </div>
        
        </Grid>
        
        </div>
        :
        <div>Please Choose a File To Preview</div>  
      }
        
          
          </div>
      
    )}
    // else{
      
    //   return <PerfectMap rawdata={this.state.response}/>
    // }
  }
}

 function mapStateToProps(state) {
   return {
     count: state
   };
 }
 function mapDispatchToProps(dispatch){
  return{
    sendRawToStore:(project)=>dispatch(sendRawToStore(project))
}
 }

 
 export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Convert)
// export default Convert;