import React, { Component } from "react"
import { compose } from "recompose"
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow,
  Polyline
} from "react-google-maps"
import polyUtil from 'polyline-encoded';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const MapWithAMarker = compose(withScriptjs, withGoogleMap)(props => { 
    
    console.log(props.zoom)
            return (
                <GoogleMap zoom={props.zoom} center={props.center}>
                    {props.markers}
                    
                    {this.props.rawdata.routes.map(element=>{
                        if(element.vehicle==props.id){
                            
                                var encoded = element.geometry;
                                var latlngs = polyUtil.decode(encoded);
                                var pathLoc=[];
                                latlngs.forEach(stop => {
                                  pathLoc.push({lat:stop[0],lng:stop[1]})
                                  
                                });
                               
                               

                                return<div> <Polyline
                                path={pathLoc}
                                geodesic={true}
                                options={{
                                    strokeColor: "#ff2527",
                                    strokeOpacity: 0.75,
                                    strokeWeight: 2,
                                    icons: [
                                        {
                                         
                                            offset: "0",
                                            repeat: "20px"
                                        }
                                    ]
                                }}
                            />  </div>
                        } 
                       

                    })}
                  
                </GoogleMap>
              )
                
       

        
      })



export default class OneMoreMap extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedMarker:props.MarkerOutstanding,
      id:this.props.id,
      unassignedMarker:null,
      MarkersOriginal:[]
    }
  }
  componentDidMount(){
    this.setState({selectedMarker:this.props.MarkerOutstanding,unassignedMarker:this.props.unassignedMarker,MarkersOriginal:[]})
  }

  handleClick = (marker, event) => {
     this.setState({ selectedMarker: marker })
     this.props.sendDataToList(marker)
    console.log(marker)
  }
  render() {
    let MarkersOriginalRender=[];
    
     
    
    
    console.log(this.state.selectedMarker)
    this.props.rawdata.routes.map((element)=>{
        if(element.vehicle==this.props.id){
            const markers0=element.steps.map((marker)=>{
              if(marker.location[1]==this.props.defaultCenter.lat){
               
            return <Marker onClick={(e)=>this.handleClick(marker)} animation={1} position={{lat:marker.location[1],lng:marker.location[0]}} >
                {this.state.selectedMarker === marker &&
              <InfoWindow>
                <div>
                <Paper >
                  
                  <Typography variant="h5" component="h3">
                   Job ID : {marker.job}
                  </Typography>
                  <Typography component="p">
                   Service Time: {marker.service/60} mins
                  </Typography>
                  <Typography component="p">
                   Arrival Time: {parseInt(marker.arrival/3600)} : {parseInt(((marker.arrival/3600)-(parseInt(marker.arrival/3600)))*60)}
                  </Typography>
              </Paper>
                </div>
              </InfoWindow>}
            }
            </Marker>}
            else{
              return <Marker onClick={(e)=>this.handleClick(marker)} position={{lat:marker.location[1],lng:marker.location[0]}} >
                {this.state.selectedMarker === marker &&
              <InfoWindow>
                <div>
                <Paper >
                  
                  <Typography variant="h5" component="h3">
                   Job ID : {marker.job}
                  </Typography>
                  <Typography component="p">
                   Service Time: {marker.service/60} mins
                  </Typography>
                  <Typography component="p">
                   Arrival Time: {parseInt(marker.arrival/3600)} : {parseInt(((marker.arrival/3600)-(parseInt(marker.arrival/3600)))*60)}
                  </Typography>
              </Paper>
                </div>
              </InfoWindow>}
            }
            </Marker>
            }
            
          })
          MarkersOriginalRender=markers0
        }
    });
     if(this.props.unassignedMarker!=null)
     MarkersOriginalRender.push(<Marker animation={2} options={{icon: {  url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"} }} position={{lat:this.props.unassignedMarker.location[1],lng:this.props.unassignedMarker.location[0]}}/>)
    
    // console.log(MarkersOriginal)
    return (
      <MapWithAMarker
        selectedMarker={this.state.selectedMarker}
        id={this.props.id}
        markers={MarkersOriginalRender}
        zoom={this.props.defaultZoom}
      center={this.props.defaultCenter}
        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyBlcSeyti4rrWc22uVDfNjmRiE_ZIH9Dc8"
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{height:`700px`,width:`750px`}} />}
        mapElement={<div style={{ height: `100%` }} />}
      />
    )
  }
}