import React, { Component } from 'react';
import Navbar from '../Navbar';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import '../Map2.css';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { ExpansionPanel} from '@material-ui/core';
import OneMoreMap from './OneMoreMap';
import DeleteIcon from '@material-ui/icons/Delete';
import Switch from '@material-ui/core/Switch';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';



const styles = theme => ({
    root: {
      flexGrow: 1,
      border:0
    },
    paper: {
      padding: theme.spacing.unit * 2,
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    expandedPanel: {
        backgroundColor: "#F0F0F0"
      },
      selected:{
          color:"white"
      } 
  });

class Test extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            id:null,
            totalTime:0,
            totalDistance:0,
            numberOfActivities:0,
            open:true,
            selected:null,
            zoom:12,
            centreLat:12.97,
            centreLong:77.59,
            selectedMarker:false,
            OpeningModalFromMarker:null,
            mainObject:[],
            unassignedObject:[],
            openVehicleMenu:false,
            newAssignedVehicle:null,
            viewToggle:true,
            unassignedMarker:null,
            openAssignDialogueBox:null,
            openDeleteDialogieBox:false,
            openAddVehicleDialogueBox:false
         }
        this.handleOnChange=this.handleOnChange.bind(this);
        this.changeMapCentre=this.changeMapCentre.bind(this);
        this.handleClose=this.handleClose.bind(this);
        this.clickSummary=this.clickSummary.bind(this);
        this.getDataFromMap=this.getDataFromMap.bind(this)
        this.handleUnassign=this.handleUnassign.bind(this)
        this.AssignmentChange=this.AssignmentChange.bind(this)
        this.AssignToTheVehicle=this.AssignToTheVehicle.bind(this)
        this.AssignmentChangeOnRow=this.AssignmentChangeOnRow.bind(this)
        this.AddNewVehicleModal=this.AddNewVehicleModal.bind(this)
        this.sendLocation=this.sendLocation.bind(this)
    }
    componentDidMount(){
        let mainObject=[]
        var totalDistance=0;
        var vehicleId=0;
        var TotalTime=0;
        var ServiceTime=0;
        var unassignedRides=[];
        
        this.props.rawdata.routes.map(element=>{
        
            {   var count=0;
                element.steps.map(()=>{
                    count=count+1;
                }
                )
                ServiceTime=(element.service)/3600;
               TotalTime=(element.steps[count-1].arrival-element.steps[0].arrival)/3600;
               var serviceTimeHours=parseInt(ServiceTime);
               var totalTimeHours=parseInt(TotalTime);
                var serviceTimeMinutes=parseInt((ServiceTime-serviceTimeHours)*60)
                var totalTimeMinutes=parseInt((TotalTime-totalTimeHours)*60);
                vehicleId=element.vehicle
                mainObject.push({VehicleId:vehicleId,TotalDistance:((element.distance)/1000),TotalTimeMinutes:totalTimeMinutes,TotalTimeHours:totalTimeHours,ServiceTimeMinutes:serviceTimeMinutes,ServiceTimeHours:serviceTimeHours,steps:element.steps})
                totalDistance=0;
            }           
        })
        this.props.rawdata.unassigned.map(element=>{
            unassignedRides.push(element)
        })
        console.log(unassignedRides)
        this.setState({
             mainObject:mainObject,
             unassignedObject:unassignedRides
         })

    }
    sendLocation(row){
        var locationArray=[];
        row.steps.map(element=>{
            locationArray.push(element.location)
        })
        console.log(locationArray)
    }
    AddNewVehicleModal(){

    }
    AssignToTheVehicle(e,element){
        e.preventDefault()
        var count=0;
        var unassigned=this.state.unassignedObject;
        unassigned.map(step=>{
            if(step.id==element.id){
                unassigned.splice(count,1)
            }
            count++;
        })
        var modifiedObject=this.state.mainObject
        modifiedObject.map((row)=>{
            
            if(row.VehicleId==this.state.newAssignedVehicle)
                row.steps.push({type:"job",job:element.id,location:element.location})
        })
        console.log(modifiedObject)
        this.setState({
            unassignedObject:unassigned,
            mainObject:modifiedObject,
            newAssignedVehicle:null,
        })
    }
    AssignmentChangeOnRow(e,element){
        console.log(element)
       this.setState({
         unassignedMarker:element,
         centreLat:element.location[1],
         centreLng:element.location[0],
         zoom:12
              })
       console.log(this.state.unassignedMarker)
    }
    AssignmentChange(e,element){
       console.log(e.target.value)
        this.setState({
            newAssignedVehicle:e.target.value,
             unassignedMarker:element
        })
        console.log(this.state.newAssignedVehicle)
        var modifiedObject=this.state.mainObject
        this.setState({
            id:e.target.value
        })
    }
    clickSummary(event,idx,location){
        console.log(idx)
        this.setState({ id: idx,
            selected:idx,
            centreLat:location[0].location[1],
            centreLong:location[0].location[0],
            zoom:12}
            );
            
            let mainObject=[]
            var totalDistance=0;
            var vehicleId=0;
            this.props.rawdata.routes.map(element=>{
                {
                    element.steps.map((step)=>{
                        totalDistance=totalDistance+step.distance
                    })
                }           
            })
            
            mainObject.push({VehicleId:vehicleId,TotalDistance:totalDistance})
    }
    handleUnassign(rowo,element){
        var appendingElement={};
        
        var modifiedObject=this.state.mainObject
        modifiedObject.map((row)=>{
            var count=0;
            row.steps.map(ele=>{
                if(ele.job==element.job){
                    row.steps.splice(count,1);
                    appendingElement.id=element.job
                    appendingElement.location=element.location
                }
                count++;
            })
        })
        console.log(appendingElement)
        var unassigned=this.state.unassignedObject;
        unassigned.push(appendingElement)
        this.setState({
             mainObject:modifiedObject,
             unassignedObject:unassigned,
             openDeleteDialogieBox:true
        })
        
    }
    handleClose(){
        this.setState({
            ModalOpen:false
        })
    }
      changeMapCentre(job,idx,lat,lng){
        this.setState(
           { id:idx,
            selected:idx,
               zoom:13,
        centreLat:lat,
        centreLong:lng,
        ModalOpen:true,
        OpeningModalFromMarker:job
        }
        )
        console.log(job)
      }
      getDataFromMap(val){
        console.log(val);
        this.setState({
            OpeningModalFromMarker:val.job
        })
      }
      handleOnChange(event,e,location) {
        event.stopPropagation();
        console.log('selected option', e);
        this.setState({ id: e,
        selected:e,
        zoom:12,
        centreLat:location[0].location[1],
        centreLong:location[0].location[0]
        }
        );
        console.log(location[0].location[0])
        let mainObject=[]
        var totalDistance=0;
        var vehicleId=0;
        this.props.rawdata.routes.map(element=>{
            {
                element.steps.map((step)=>{
                    totalDistance=totalDistance+step.distance
                })
            }           
        })
        
        mainObject.push({VehicleId:vehicleId,TotalDistance:totalDistance})
        console.log(mainObject)
      }
      
    render() { 
       const { classes } = this.props;
       var countVar=-1;
        const {mainObject}=this.state;
        // this.state.unassignedMarker=null
        console.log(mainObject)
        console.log(this.state.OpeningModalFromMarker);
        console.log(this.props.count)
        return ( 
            
            <div>
                <Navbar />
                
                <Grid container spacing={24}>
                <Grid item xs={6}>

                    <OneMoreMap id={this.state.id}
                                defaultCenter={{lat:this.state.centreLat,lng:this.state.centreLong}} 
                                defaultZoom={this.state.zoom} 
                                sendDataToList={this.getDataFromMap}
                                MarkerOutstanding={this.state.OpeningModalFromMarker}
                                unassignedMarker={this.state.unassignedMarker}
                                rawdata={this.props.rawdata}/>
                </Grid>
                <Grid item xs={6}>
               
                    <div>    
                        
                <div style={{textAlign:`center`}}>  Vehicle List <Switch color="default" onClick={(e)=>{this.setState({viewToggle:!this.state.viewToggle,id:null,unassignedMarker:null})}} /> Unassigned List </div> 
                    
                    {this.state.viewToggle?(<Table style={{border:`0px`}}>
                        <Grid className="scroll" style={{height:`700px`}}>
                            
                                <TableHead>
                                    <TableRow >
                                        <TableCell >Vehicle Id</TableCell>
                                        <TableCell >Total Distance</TableCell>
                                        <TableCell >Total Time</TableCell>
                                        <TableCell>Service Time</TableCell>
                                    </TableRow>
                                </TableHead>
                                <div>
                                <TableBody >
                                    {mainObject.map(row => 
                                        {if(this.state.selected==row.VehicleId){
                                             
                                    return  <ExpansionPanel>
                                                <ExpansionPanelSummary style={{width:`100%`}} expandIcon={<ExpandMoreIcon />} value={row.VehicleId}  onClick={(e)=>this.clickSummary(e,row.VehicleId,row.steps)} classes={{ expanded: classes.expandedPanel }}>
                                                    <TableRow selected hover className={{selected:classes.expandedPanel}} style={{textAlign:`centre`}} value={row.VehicleId} onClick={(e)=>this.handleOnChange(e,row.VehicleId,row.steps) } >
                                                        <TableCell style={{borderBottom:`0px`,paddingTop:`15px`}} component="th" scope="row">
                                                            {row.VehicleId}
                                                        </TableCell>
                                                        <TableCell style={{borderBottom:`0px`,paddingTop:`15px`}}>{row.TotalDistance} km</TableCell>
                                                        <TableCell style={{borderBottom:`0px`,paddingTop:`15px`}}>{row.TotalTimeHours} Hrs {row.TotalTimeMinutes} Min</TableCell>
                                                        <TableCell style={{borderBottom:`0px`,paddingTop:`15px`}}>{row.ServiceTimeHours} Hrs {row.ServiceTimeMinutes} Min</TableCell>
                                                    </TableRow>
                                                </ExpansionPanelSummary>
                                                <ExpansionPanelDetails>
                                                    <Table>
                                                        <TableHead>
                                                            <div>
                                                                <TableCell style={{paddingLeft:`20px`}} >Job Id</TableCell>
                                                                <TableCell style={{paddingLeft:`170px`}}>Arrival</TableCell>
                                                                <TableCell style={{paddingLeft:`150px`}}>Service</TableCell>
                                                            </div>
                                                        </TableHead>
                                                        <div>
                                                        <TableBody>
                                                            
                                                            {   
                                                                row.steps.map((expandingElement)=>{
                                                                    countVar=countVar+1;
                                                                    var arrivalTime=expandingElement.arrival/3600;
                                                                    var arrivalTimeHours=parseInt(arrivalTime);
                                                                    var arrivalTimeMinutes=parseInt((arrivalTime-arrivalTimeHours)*60)
                                                                    if(expandingElement.type=="job")
                                                                    {  console.log(this.state.OpeningModalFromMarker)
                                                                        if(this.state.OpeningModalFromMarker==expandingElement.job)
                                                                    {
                                                                        return <ExpansionPanel expanded={true} key={countVar}>
                                                                        <ExpansionPanelSummary expandIcon={<DeleteIcon onClick={(e)=>this.handleUnassign(row,expandingElement)}  />} value={row.VehicleId}  style={{width:`650px`,height:`10px`}}>
                                                                    <TableRow style={{height:`27px`}} onClick={(e)=>this.changeMapCentre(expandingElement,row.VehicleId,expandingElement.location[1],expandingElement.location[0])}>
                                                                                        
                                                                                            
                                                                                        <TableCell style={{borderBottom:`0px`,paddingBottom:`13px`,textAlign:`centre`}} component="th" scope="row">{expandingElement.job}</TableCell>
                                                                                        <TableCell style={{paddingLeft:`140px`,borderBottom:`0px`,paddingBottom:`13px`,textAlign:`centre`}}>{arrivalTimeHours} : {arrivalTimeMinutes}</TableCell>
                                                                                        <TableCell style={{paddingLeft:`140px`,borderBottom:`0px`,paddingBottom:`13px`,textAlign:`centre`}}>{(expandingElement.service)/60} Mins</TableCell>
                                                                                     
                                                                                        
                                                                                       
                                                                                        
                                                                            </TableRow>
                                                                           
                                                                            </ExpansionPanelSummary>   
                                                                            <Dialog
                                                                                open={this.state.openDeleteDialogieBox}
                                                                                onClose={(e)=>{this.setState({openDeleteDialogieBox:false})}}
                                                                                aria-labelledby="alert-dialog-title"
                                                                                aria-describedby="alert-dialog-description"
                                                                            >
                                                                                <DialogTitle id="alert-dialog-title">{"Use Google's location service?"}</DialogTitle>
                                                                                <DialogContent>
                                                                                <DialogContentText id="alert-dialog-description">
                                                                                    Let Google help apps determine location. This means sending anonymous location data to
                                                                                    Google, even when no apps are running.
                                                                                </DialogContentText>
                                                                                </DialogContent>
                                                                                <DialogActions>
                                                                                
                                                                                <Button onClick={(e)=>{this.setState({openDeleteDialogieBox:false})}} color="primary" autoFocus>
                                                                                    Done
                                                                                </Button>
                                                                                </DialogActions>
                                                                            </Dialog>
                                                                                        <ExpansionPanelDetails style={{width:`650px`,height:`20px`,marginBottom:`9px`}}>
                                                                                            <TableRow>
                                                                                                
                                                                                               <TableCell style={{borderBottom:`0px`,paddingBottom:`13px`,textAlign:`centre`}}>Latitude: {expandingElement.location[1]} </TableCell>
                                                                                               <TableCell style={{borderBottom:`0px`,paddingBottom:`13px`,textAlign:`centre`}}>Longitude: {expandingElement.location[0]}</TableCell>
                                                                                               <TableCell style={{borderBottom:`0px`,paddingBottom:`13px`,textAlign:`centre`}}>Stop No: {countVar} </TableCell>
                                                                                            </TableRow>
                                                                                        </ExpansionPanelDetails>
                                                                    </ExpansionPanel>   }
                                                                    else{
                                                                    return <ExpansionPanel >
                                                                        <ExpansionPanelSummary expandIcon={<DeleteIcon onClick={(e)=>{this.handleUnassign(row,expandingElement)}} />} value={row.VehicleId} style={{width:`650px`,height:`10px`}}>
                                                                    <TableRow style={{height:`27px`}} onClick={(e)=>this.changeMapCentre(expandingElement,row.VehicleId,expandingElement.location[1],expandingElement.location[0])} style={{textAlign:`centre`,paddingTop:`2%`}} >
                                                                                        
                                                                                            
                                                                                        <TableCell style={{borderBottom:`0px`,paddingBottom:`13px`,textAlign:`centre`}} component="th" scope="row">{expandingElement.job}</TableCell>
                                                                                        <TableCell style={{paddingLeft:`140px`,borderBottom:`0px`,paddingBottom:`13px`,textAlign:`centre`}}>{arrivalTimeHours} : {arrivalTimeMinutes}</TableCell>
                                                                                        <TableCell style={{paddingLeft:`140px`,borderBottom:`0px`,paddingBottom:`13px`,textAlign:`centre`}}>{(expandingElement.service)/60} Mins</TableCell>
                                                        
                                                                                       
                                                                                        
                                                                            </TableRow>
                                                                            
                                                                            </ExpansionPanelSummary>   
                                                                            <Dialog
                                                                                open={this.state.openDeleteDialogieBox}
                                                                                onClose={(e)=>{this.setState({openDeleteDialogieBox:false})}}
                                                                                aria-labelledby="alert-dialog-title"
                                                                                aria-describedby="alert-dialog-description"
                                                                            >
                                                                                <DialogTitle id="alert-dialog-title">{"Job Unassigned"}</DialogTitle>
                                                                                <DialogContent>
                                                                                <DialogContentText id="alert-dialog-description">
                                                                                   The Job of Vehicle {this.state.id} has been Unassigned
                                                                                </DialogContentText>
                                                                                </DialogContent>
                                                                                <DialogActions>
                                                                                
                                                                                <Button onClick={(e)=>{this.setState({openDeleteDialogieBox:false,viewToggle:false})}} color="primary" autoFocus>
                                                                                    OK
                                                                                </Button>
                                                                                </DialogActions>
                                                                            </Dialog>
                                                                                        <ExpansionPanelDetails style={{width:`650px`,height:`20px`,marginBottom:`9px`}}>
                                                                                            <TableRow style={{textAlign:`centre`}} >
                                                                                                
                                                                                               <TableCell style={{borderBottom:`0px`,paddingBottom:`13px`,textAlign:`centre`}}>Latitude: {expandingElement.location[1]} </TableCell>
                                                                                               <TableCell style={{borderBottom:`0px`,paddingBottom:`13px`,textAlign:`centre`}}>Longitude: {expandingElement.location[0]}</TableCell>
                                                                                               <TableCell style={{borderBottom:`0px`,paddingBottom:`13px`,textAlign:`centre`}}>Stop No: {countVar} </TableCell>
                                                                                            </TableRow>
                                                                                        </ExpansionPanelDetails>
                                                                    </ExpansionPanel>   }

                                                                }
                                                                       
                                                                    
                                                                
                                                            })
                                                            }
                                                        </TableBody>   
                                                        </div>  
                                                    </Table>
                                                    <Button onClick={this.sendLocation(row)}>Ok</Button>
                                                </ExpansionPanelDetails>
                                            </ExpansionPanel> }
                                    else{
                                    return <ExpansionPanel>
                                                <ExpansionPanelSummary style={{width:`720px`}} expandIcon={<ExpandMoreIcon />} onClick={(e)=>this.clickSummary(e,row.VehicleId,row.steps)} classes={{ expanded: classes.expandedPanel }}>
                                                    <TableRow hover classes={{selected:classes.expandedPanel}} value={row.VehicleId} onClick={(e)=>this.handleOnChange(e,row.VehicleId,row.steps) } style={{textAlign:`centre`}} >
                                                        <TableCell style={{borderBottom:`0px`,paddingTop:`15px`,textAlign:`centre`}} component="th" scope="row">
                                                            {row.VehicleId}  
                                                        </TableCell>
                                                        <TableCell style={{borderBottom:`0px`,paddingTop:`15px`,paddingLeft:`50px`}}>{row.TotalDistance} km</TableCell>
                                                        <TableCell style={{borderBottom:`0px`,paddingTop:`15px`}}>{row.TotalTimeHours} Hrs {row.TotalTimeMinutes} Min</TableCell>
                                                        <TableCell style={{borderBottom:`0px`,paddingTop:`15px`}}>{row.ServiceTimeHours} Hrs {row.ServiceTimeMinutes} Min</TableCell>
                                                    </TableRow>
                                                </ExpansionPanelSummary>
                                                <ExpansionPanelDetails>
                                                    <Table>
                                                        <TableHead>
                                                            <TableRow>
                                                                <TableCell>Job Id</TableCell>
                                                                <TableCell >Arrival</TableCell>
                                                                <TableCell >Service</TableCell>
                                                            </TableRow>
                                                        </TableHead>
                                                        <TableBody>
                                                            {
                                                                row.steps.map((expandingElement)=>{
                                                                    var arrivalTime=expandingElement.arrival/3600;
                                                                    var arrivalTimeHours=parseInt(arrivalTime);
                                                                    var arrivalTimeMinutes=parseInt((arrivalTime-arrivalTimeHours)*60)
                                                                    if(expandingElement.type=="job")
                                                                    return <TableRow onClick={(e)=>this.changeMapCentre(expandingElement,row.VehicleId,expandingElement.location[1],expandingElement.location[0])}>
                                                                                <TableCell>{expandingElement.job}</TableCell>
                                                                                <TableCell>{arrivalTimeHours} : {arrivalTimeMinutes}</TableCell>
                                                                                <TableCell>{(expandingElement.service)/60} Mins</TableCell>
                                                                                <DeleteIcon />
                                                                            </TableRow>
                                                                    })
                                                            }
                                                        </TableBody>     
                                                    </Table>
                                                </ExpansionPanelDetails>
                                            </ExpansionPanel>
                                    }    
                        }
            )}
                                </TableBody>
                            </div>    
                        
                    </Grid>
                </Table>):(<div><Table>
                    <div className="scroll" style={{height:`620px`}}>
            <TableHead>
                <TableRow>
                    <TableCell >Job Id</TableCell>
                    <TableCell >Where Do You Want To Assign It?</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
            
                {
                this.state.unassignedObject.map(element=>{
                    let selectedVehicle=element.id;
                    return <TableRow onClick={(e)=>this.AssignmentChangeOnRow(e,element)}>
                        <TableCell>{element.id}</TableCell>
                        <TableCell >
                            <form onSubmit={(e)=>this.AssignToTheVehicle(e,element)}>
                                
                                 <select name='vehicle' onChange={(e)=>this.AssignmentChange(e,element)}>
                                     <option value='' selected>Choose Vehicle</option>
                                {this.state.mainObject.map(ele=>{
                                    
                                    return <option value={ele.VehicleId} >{ele.VehicleId}</option>
                                        })}
                                </select>
                               
                            
                            


                                <button onClick={(e)=>{this.setState({openAssignDialogueBox:true})}}>OK</button>
                                
                                
                            </form>
                            </TableCell>
                        </TableRow>
                
                })
                
            }
                    
                    <Dialog
                            open={this.state.openAssignDialogueBox}
                            onClose={(e)=>{this.setState({openAssignDialogueBox:false})}}
                            aria-labelledby="alert-dialog-title"
                            aria-describedby="alert-dialog-description"
                        >
                            <DialogTitle id="alert-dialog-title">{"Vehicle Assigned"}</DialogTitle>
                            <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                               The Job has been assigned to Vehicle {this.state.id} 
                            </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                            <Button onClick={(e)=>{this.setState({openAssignDialogueBox:null,id:null,unassignedMarker:null,newAssignedVehicle:null,selectedMarker:null,OpeningModalFromMarker:null})}} type="submit" color="primary" autoFocus>
                                Done
                            </Button>
                            </DialogActions>
                        </Dialog>
            </TableBody>
            </div>
        </Table> 
        <div>Map rendered : {this.state.id}</div>
        </div>)}
                    
            </div>
        </Grid>
    </Grid>
</div>
         );
    }
}
 


function mapStateToProps(state) {
    console.log(state);
    return {
      count: state
    };
  }

export default connect(
    mapStateToProps,
  )(withStyles(styles)(Test));