import React, { Component } from 'react';
import Convert from './Convert';
import Popup from "reactjs-popup";
import "./popup.css";
import Button from '@material-ui/core/Button';
import Navbar from './Navbar';
import FormControl from '@material-ui/core/FormControl'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import AddIcon from '@material-ui/icons/Add';
import PerfectMap from './components/PerfectMap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


class Popupexp extends Component {
    constructor(props) {
        super(props);
        this.state = { 
          capacityArray:[{}],
          displayUpload:true,
          displayMapView:false,
          rawdata:null,
          openValidationDialogue:false
         }
        this.addCapacity=this.addCapacity.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
        this.handleChange=this.handleChange.bind(this);
        this.handleRawdata=this.handleRawdata.bind(this);
    }
    //this function renders the map view when rawdata is recieved
    handleRawdata(value){
      this.setState({displayMapView:true})

    }
    //On Clicking Add Category Button, this function adds a new category
   addCapacity(e){
     e.preventDefault()
    this.setState((prevState) => ({
      capacityArray: [...prevState.capacityArray, {capacity:"", numberOfVehicles:"",location:"",startTime:"",endTime:""}],
    }));
   }
   //this function saves basic form details
   handleChange = (e) => {
     
    if (["capacity", "numberOfVehicles","location","startTime","endTime"].includes(e.target.className) ) {
      let capacityArray = [...this.state.capacityArray]   
      capacityArray[e.target.dataset.id][e.target.className] = e.target.value
      this.setState({ capacityArray }, () => console.log(this.state.capacityArray))
    } else {
      this.setState({ [e.target.name]: e.target.value })
    }
  }
  //this function handles submit. It sets the state and we see Upload page
   handleSubmit = (e) => { e.preventDefault()
    console.log(this.state.capacityArray)
    this.state.capacityArray.map(object=>{
      if(Object.keys(object).length!=5){
        this.setState({openValidationDialogue:true})
      }
      else{
        this.setState({displayUpload:!this.state.displayUpload})
      }
    })
      
    }


    render() { 
          const contentStyle = {
            width: "90%",
              height: "600px",
              overflow:"hidden"
            };
      if(this.state.displayMapView==false)
                    {return ( 
                        <div >
                          <Navbar />
                          <div style={{textAlign:`center`}}>
                    {this.state.displayUpload   ?   
                        <FormControl style={{textAlign:`center`,marginTop:`10%`,backgroundColor:``,borderStyle:`none`,borderWidth:`2px`}} onChange={this.handleChange}>
                            <label htmlFor="capacity" style={{fontWeight:`700`,fontSize:`25px`}}>  Please Add the Following Details  </label> <br />
                            <Button  style={{marginLeft:`10%`,marginRight:`10%`,backgroundColor:`#E9E9E9`,marginBottom:`3%`}} onClick={this.addCapacity}>Add a new Category<AddIcon style={{marginLeft:`7px`}} /></Button>
                                {
                                  this.state.capacityArray.map((val, idx)=> {
                                    let capacityId = `cat-${idx}`, numberOfVehiclesId = `age-${idx}`,locationId=`location-${idx}`,startTimeId=`starttime-${idx}`,endTimeId=`endtime-${idx}`
                                    return (
                                      <div key={idx}>
                                        <label htmlFor={capacityId}>Loading Capacity(ccc)</label>
                                        <input
                                          type="number"
                                          name={capacityId}
                                          data-id={idx}
                                          id={capacityId}
                                          className="capacity"
                                          style={{width:`80px`,margin:`10px`}}
                                          required
                                        />
                                        <label htmlFor={numberOfVehiclesId}>No. Of Vehicles</label>
                                        <input
                                          type="number"
                                          name={numberOfVehiclesId}
                                          data-id={idx}
                                          id={numberOfVehiclesId}
                                          style={{width:`80px`,margin:`10px`}}
                                          className="numberOfVehicles"
                                          required
                                        />
                                        <label htmlFor={locationId}>Warehouse Location(Lat,Long)</label>
                                        <input
                                          type="text"
                                          name={locationId}
                                          data-id={idx}
                                          id={locationId}
                                          className="location"
                                          style={{width:`80px`,margin:`10px`}}
                                          required
                                        />
                                        <label htmlFor={startTimeId}>Vehicle Availability Spot</label>
                                        <input
                                          type="text"
                                          name={startTimeId}
                                          data-id={idx}
                                          id={startTimeId}
                                          className="startTime"
                                          style={{width:`50px`,margin:`10px`,}}
                                          required
                                        />-
                                        <input
                                          type="text"
                                          name={endTimeId}
                                          data-id={idx}
                                          id={endTimeId}
                                          className="endTime"
                                          style={{width:`50px`,margin:`10px`}}
                                          required
                                        /> 
                                      </div>
                                    )
                                  })
                                }
                        <Button type="submit" onClick={this.handleSubmit} style={{backgroundColor:`#E9E9E9`,marginTop:`3%`}} value="Submit" >Submit</Button> 
                        <Dialog open={this.state.openValidationDialogue} onClose={(e)=>{this.setState({openValidationDialogue:false})}}>
                          <DialogTitle id="alert-dialog-title">{"Please Fill the Form Correctly"}</DialogTitle>
                          <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                              You Have Left Some Fields Unfilled. Please Make sure you fill all the fields.
                            </DialogContentText>
                          </DialogContent>
                        </Dialog>
                      </FormControl>
                  : 
                  <div style={{textAlign:`center`}}>
                    <div style={{textAlign:`center`,display:`flex`,alignItems:`center`,justifyContent:`center`}}>
                    <Table style={{marginTop:`7%`,width:`600px`,borderTop:`5px`}}>
                      <TableHead>
                        <TableRow>
                          <TableCell>
                            Capacity
                          </TableCell>
                          <TableCell>
                            No. Of Vehicles
                          </TableCell>
                          <TableCell>
                            Location
                          </TableCell>
                          <TableCell>
                            Shift
                          </TableCell>

                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {this.state.capacityArray.map((element)=>{
                          return(<TableRow>
                                  <TableCell>{element.capacity}</TableCell>
                                  <TableCell>{element.numberOfVehicles}</TableCell>
                                  <TableCell>{element.location}</TableCell>
                                  <TableCell>{element.startTime}-{element.endTime}</TableCell>
                                </TableRow>)
                              }
                            )
                        }
                      </TableBody>
                    </Table>
                    </div>
                    <Popup trigger={<Button  style={{marginTop:`10%`,backgroundColor:"#2c4a70",fontWeight:`500`,
                            color:"white",width: "250px",
                            height: "80px",}} >Upload</Button>} modal contentStyle={contentStyle}>
                          {close => (
                            <div className="modal">
                            <a className="close" onClick={close}>
                            &times;
                            </a>
                            <Convert addedInputs={this.state.capacityArray} onSelectRawdata={this.handleRawdata}  className="popup-excel" />
                            </div>
                          )}
                    </Popup>
                    <Button style={{marginTop:`10%`,marginLeft:`5%`,backgroundColor:"#2c4a70",
                        color:"white",width: "250px",
                        height: "80px",fontWeight:`500`}} onClick={(e)=>{this.setState({displayUpload:true})}}>
                      Change Vehicle Details
                    </Button>
                  </div>
              } 
              </div>
                    
                  
          </div>
         );
        }

         else{
         return <PerfectMap rawdata={this.state.rawdata} />
         }
    }
}
 
export default Popupexp;